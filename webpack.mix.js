const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.webpackConfig({
  resolve: {
    alias: {
      '~': path.resolve(__dirname, 'resources/app/admin')
    }
  }
})

mix.autoload({
  jquery: ['$', 'jQuery', 'window.jQuery'], popper: ['Popper', 'window.Popper'],
})

mix.js('resources/app/admin/index.js', 'public/js/app.js')
  .sass('resources/app/admin/assets/sass/app.scss', 'public/css')
  .version()

mix.copyDirectory('resources/app/common/limitless/images', 'public/images')
  .version()
