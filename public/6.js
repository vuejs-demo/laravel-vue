(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-4" }, [
        _c(
          "div",
          { staticClass: "card card-body border-top-teal text-center" },
          [
            _c("h6", { staticClass: "m-0 font-weight-semibold" }, [
              _vm._v("Bordered pagination")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3 text-muted" }, [
              _vm._v("Default bordered pagination")
            ]),
            _vm._v(" "),
            _c("ul", { staticClass: "pagination align-self-center" }, [
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("←   Prev")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item active" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("1")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("2")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item disabled" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("3")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("4")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("Next   →")
                ])
              ])
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-4" }, [
        _c(
          "div",
          { staticClass: "card card-body border-top-teal text-center" },
          [
            _c("h6", { staticClass: "m-0 font-weight-semibold" }, [
              _vm._v("Flat pagination")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3 text-muted" }, [
              _vm._v("Using "),
              _c("code", [_vm._v("pagination-flat")]),
              _vm._v(" class")
            ]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "pagination pagination-flat align-self-center" },
              [
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("←   Prev")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item active" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("1")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("2")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item disabled" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("3")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("4")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("Next   →")
                  ])
                ])
              ]
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-4" }, [
        _c(
          "div",
          { staticClass: "card card-body border-top-teal text-center" },
          [
            _c("h6", { staticClass: "m-0 font-weight-semibold" }, [
              _vm._v("Separated pagination")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-3 text-muted" }, [
              _vm._v("Using "),
              _c("code", [_vm._v("pagination-separated")]),
              _vm._v(" class")
            ]),
            _vm._v(" "),
            _c(
              "ul",
              {
                staticClass: "pagination pagination-separated align-self-center"
              },
              [
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("←   Prev")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item active" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("1")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("2")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item disabled" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("3")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("4")
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "page-item" }, [
                  _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                    _vm._v("Next   →")
                  ])
                ])
              ]
            )
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/app/admin/views/account/Test.vue":
/*!****************************************************!*\
  !*** ./resources/app/admin/views/account/Test.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Test.vue?vue&type=template&id=caf0d70a& */ "./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a&");
/* harmony import */ var _Test_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Test.vue?vue&type=script&lang=js& */ "./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Test_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/app/admin/views/account/Test.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Test_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Test.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/account/Test.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Test_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a&":
/*!***********************************************************************************!*\
  !*** ./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Test.vue?vue&type=template&id=caf0d70a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/account/Test.vue?vue&type=template&id=caf0d70a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Test_vue_vue_type_template_id_caf0d70a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);