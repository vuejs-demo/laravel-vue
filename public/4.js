(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "content d-flex justify-content-center align-items-center"
      },
      [
        _c("form", { staticClass: "login-form", attrs: { action: "" } }, [
          _c("div", { staticClass: "card mb-0" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "text-center mb-3" }, [
                _c("i", {
                  staticClass:
                    "icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"
                }),
                _vm._v(" "),
                _c("h5", { staticClass: "mb-0" }, [
                  _vm._v("Login to your account")
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "d-block text-muted" }, [
                  _vm._v("Your credentials")
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "form-group form-group-feedback form-group-feedback-left"
                },
                [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: { type: "text", placeholder: "Username" }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-control-feedback" }, [
                    _c("i", { staticClass: "icon-user text-muted" })
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "form-group form-group-feedback form-group-feedback-left"
                },
                [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: { type: "password", placeholder: "Password" }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-control-feedback" }, [
                    _c("i", { staticClass: "icon-lock2 text-muted" })
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group d-flex align-items-center" },
                [
                  _c("div", { staticClass: "form-check mb-0" }, [
                    _c("label", { staticClass: "form-check-label" }, [
                      _c("input", {
                        staticClass: "form-input-styled",
                        attrs: {
                          type: "checkbox",
                          name: "remember",
                          checked: "",
                          "data-fouc": ""
                        }
                      }),
                      _vm._v(
                        "\n                            Remember\n                        "
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("a", { staticClass: "ml-auto", attrs: { href: "" } }, [
                    _vm._v("Forgot password?")
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-block",
                    attrs: { type: "submit" }
                  },
                  [
                    _vm._v("Sign in "),
                    _c("i", { staticClass: "icon-circle-right2 ml-2" })
                  ]
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "form-group text-center text-muted content-divider"
                },
                [
                  _c("span", { staticClass: "px-2" }, [
                    _vm._v("or sign in with")
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "form-group text-center" }, [
                _c(
                  "button",
                  {
                    staticClass:
                      "btn btn-outline bg-indigo border-indigo text-indigo btn-icon rounded-round border-2",
                    attrs: { type: "button" }
                  },
                  [_c("i", { staticClass: "icon-facebook" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass:
                      "btn btn-outline bg-pink-300 border-pink-300 text-pink-300 btn-icon rounded-round border-2 ml-2",
                    attrs: { type: "button" }
                  },
                  [_c("i", { staticClass: "icon-dribbble3" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass:
                      "btn btn-outline bg-slate-600 border-slate-600 text-slate-600 btn-icon rounded-round border-2 ml-2",
                    attrs: { type: "button" }
                  },
                  [_c("i", { staticClass: "icon-github" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass:
                      "btn btn-outline bg-info border-info text-info btn-icon rounded-round border-2 ml-2",
                    attrs: { type: "button" }
                  },
                  [_c("i", { staticClass: "icon-twitter" })]
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "form-group text-center text-muted content-divider"
                },
                [
                  _c("span", { staticClass: "px-2" }, [
                    _vm._v("Don't have an account?")
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "a",
                  {
                    staticClass: "btn btn-light btn-block",
                    attrs: { href: "#" }
                  },
                  [_vm._v("Sign up")]
                )
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "form-text text-center text-muted" }, [
                _vm._v(
                  "By continuing, you're confirming that you've read our "
                ),
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Terms & Conditions")
                ]),
                _vm._v(" and "),
                _c("a", { attrs: { href: "#" } }, [_vm._v("Cookie Policy")])
              ])
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/app/admin/views/auth/Login.vue":
/*!**************************************************!*\
  !*** ./resources/app/admin/views/auth/Login.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=27b84609& */ "./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/app/admin/views/auth/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609&":
/*!*********************************************************************************!*\
  !*** ./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=27b84609& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/app/admin/views/auth/Login.vue?vue&type=template&id=27b84609&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_27b84609___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);