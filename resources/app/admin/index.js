import Vue from 'vue'

import i18n from '../common/locales'
import './init'
import AppContainer from './views/App'
import router from './routes'
import store from './store'

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(AppContainer),
});
