import Vue from 'vue'
import Router from 'vue-router'
import tokenService from '~/services/token'

import DefaultContainer from '~/layouts/DefaultContainer'
import Auth from './modules/auth'
import Dashboard from './modules/dashboard'
import Account from './modules/account'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    redirect: '/dashboard',
    name: 'index',
    component: DefaultContainer,
    children: [
      Dashboard,
      Account,
    ]
  },
  Auth,
  {
    path: '*',
    component: () => import('../views/errors/404')
  }
]

const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap,
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',

})

router.beforeEach((to, from, next) => {
  // const isPublic = to.matched.some(record => record.meta.public)
  // const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut)
  // const loggedIn = !!tokenService.getToken()
  //
  // if (!isPublic && !loggedIn) {
  //   return next({
  //     path:'/login',
  //     query: {redirect: to.fullPath}  // Store the full path to redirect the user to after login
  //   });
  // }
  //
  // // Do not allow user to visit login page or register page if they are logged in
  // if (loggedIn && onlyWhenLoggedOut) {
  //   return next('/')
  // }

  to.meta.transitionName = null

  next()
})

router.beforeResolve((to, from, next) => {
  if (to.name) {
    const toDepth = to.path.split('/').length
    const fromDepth = from.path.split('/').length
    to.meta.transitionName = toDepth < fromDepth ? 'slide-right' : 'slide-left'
  }
  next()
})

router.afterEach((to, from) => {


})

export default router