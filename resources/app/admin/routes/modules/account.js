const AccountIndex = () => import('~/views/account/Index')
const AccountTest = () => import('~/views/account/Test')

export default {
  path: 'account',
  component: {
    render (c) { return c('router-view') }
  },
  children: [
    {
      path: '',
      name: 'account.index',
      component: AccountIndex,
    },
    {
      path: 'test',
      name: 'account.test',
      component: AccountTest,
    },
  ]
}