import AuthContainer from '~/layouts/AuthContainer'
import Login from '~/views/auth/Login'

export default {
  path: '/login',
  component: AuthContainer,
  children: [
    {
      path: '',
      component: Login,
      name: 'auth.login',
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      },
    }
  ]
}