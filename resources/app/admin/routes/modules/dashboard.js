const DashboardIndex = () => import('../../views/dashboard/Index')

export default {
  path: 'dashboard',
  component: {
    render (c) { return c('router-view') }
  },
  children: [
    {
      path: '',
      name: 'dashboard.index',
      component: DashboardIndex
    }
  ]
}