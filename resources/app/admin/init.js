import Vue from 'vue'
import lodash from 'lodash'
import io from 'socket.io-client'

import DefaultContainer from '~/layouts/DefaultContainer'
import AuthContainer from '~/layouts/AuthContainer'

import VeeValidate, {Validator} from 'vee-validate'

import '../common/plugins/jquery.uniform'
import 'bootstrap'


class Init {
  constructor() {
    this.initLoad()

    this.configGlobal()
    this.configCustomValidate()
  }

  initLoad = () => {
    window._ = lodash
    window.socket = io('http://localhost:3000')
  }

  configGlobal = () => {
    Vue.component('default-container', DefaultContainer)
    Vue.component('auth-container', AuthContainer)
  }

  configCustomValidate = () => {
    Vue.use(VeeValidate)
    this.customRuleValidate()

    // Validator.localize('vi', jaDict)
  }

  /**
   * Custom rule validate
   */
  customRuleValidate = () => {

    // Custom validate email address
    Validator.extend('email', {
      validate: (value) => {
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return regex.test(String(value).toLowerCase())
      }
    })


  }
}

new Init()
