import {REFRESH_TOKEN_KEY, TOKEN_KEY, AUTH_INFO} from "../../common/helpers/constants";

export default {
  getToken: () => {
    return localStorage.getItem(TOKEN_KEY)
  },

  saveToken: (accessToken) => {
    localStorage.setItem(TOKEN_KEY, accessToken)
  },

  removeToken: () => {
    localStorage.removeItem(TOKEN_KEY)
  },

  getRefreshToken: () => {
    return localStorage.getItem(REFRESH_TOKEN_KEY)
  },

  saveRefreshToken: (refreshToken) => {
    localStorage.setItem(REFRESH_TOKEN_KEY, refreshToken)
  },

  removeRefreshToken: () => {
    localStorage.removeItem(REFRESH_TOKEN_KEY)
  },

  saveAuthInfo: (user) => {
    localStorage.setItem(AUTH_INFO, JSON.stringify(user))
  },

  getAuthInfo: () => {
    return JSON.parse(localStorage.getItem(AUTH_INFO))
  },

  removeAuthInfo: () => {
    localStorage.removeItem(AUTH_INFO)
  }
}
