import Vue from 'vue'
import VueI18n from 'vue-i18n'

import lang_en from './en'
import lang_vi from './vi'

Vue.use(VueI18n)

const messages = {
    en: {
        ...lang_en,
    },
    vi: {
        ...lang_vi,
    },
}

const DEFAULT_LOCALE = 'vi'

/**
 * Define I18n instance.
 *
 * @type {VueI18n}
 */
const i18n = new VueI18n({
    locale: process.env.MIX_LOCALE || DEFAULT_LOCALE,
    fallbackLocale: DEFAULT_LOCALE,
    messages,
    silentTranslationWarn: true
})

window.trans = (key) => {
    return i18n.t(key)
}

export default i18n
