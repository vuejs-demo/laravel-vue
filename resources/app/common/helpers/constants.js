export default {
  ROLES: {
    OPERATOR: 1,
    REPORTER: 2
  },
  PERMISSION: {},

  USER: {
    STATUS: {
      ACTIVE: 1,
      NON_ACTIVE: 2,
    }
  }
}

export const TOKEN_KEY = 'admin_access_token'
export const TOKEN_TYPE = 'admin_token_type'
export const REFRESH_TOKEN_KEY = 'admin_refresh_token'
export const AUTH_INFO = 'admin_auth_info'
