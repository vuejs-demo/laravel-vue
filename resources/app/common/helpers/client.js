import axios from 'axios/index'
import NProgress from 'NProgress'

function getToken() {
  return localStorage.getItem('token');
}

/**
 *
 * parse error response
 */
function parseError(messages) {
  // error
  if (messages) {
    if (messages instanceof Array) {
      return Promise.reject(messages)
    } else {
      return Promise.reject(messages)
    }
  } else {
    return Promise.reject([])
  }
}

/**
 * parse response
 */
function parseBody(response) {
  if (response.status === 200 && response.data.status) {
    return response.data
  } else {
    return parseError(response.data.message)
  }
}

/**
 * axios instance
 */
const calculatePercentage = (loaded, total) => (Math.floor(loaded * 1.0) / total)

const setupUpdateProgress = () => {
  const update = e => NProgress.inc(calculatePercentage(e.loaded, e.total))
  instance.defaults.onDownloadProgress = update
  // instance.defaults.onUploadProgress = update
}

let requestsCounter = 0
let instance = axios.create({
  baseURL: '/api/v1'
})

// request header
instance.interceptors.request.use((config) => {
  // Do something before request is sent
  if (!config.headers.Authorization) {
    const token = getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
  }
  if (requestsCounter == 0)
    NProgress.start()

  requestsCounter++
  return config
}, error => {
  return Promise.reject(error)
})
setupUpdateProgress()
// response parse
instance.interceptors.response.use((response) => {
  if ((--requestsCounter) === 0) {
    NProgress.done(true)
  }
  return parseBody(response)
}, error => {
  if ((--requestsCounter) === 0) {
    NProgress.done(true)
  }
  if (error.response.data.errors) {
    return parseError(error.response.data.errors)
  } else if (error.response.data.message) {
    return parseError(error.response.data.message)
  } else {
    return parseError(error)
  }
})

export const client = instance
